/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/29 16:02:16 by cpestour          #+#    #+#             */
/*   Updated: 2015/01/03 17:46:26 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strsub(char const *s, unsigned int start, size_t len)
{
	size_t	i;
	char	*tronc;

	if (s)
	{
		if ((tronc = (char *)malloc(sizeof(char) * (len + 1))) == NULL)
			return (NULL);
		i = 0;
		while (i < len)
		{
			tronc[i] = s[start + i];
			i++;
		}
		tronc[i] = '\0';
		return (tronc);
	}
	else
		return (NULL);
}
