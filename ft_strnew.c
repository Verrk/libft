/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/29 15:21:31 by cpestour          #+#    #+#             */
/*   Updated: 2015/01/15 14:26:28 by verrk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strnew(size_t size)
{
	void	*new;

	if (size <= 0)
		return (NULL);
	new = malloc(sizeof(char) * (size + 1));
	ft_bzero(new, size + 1);
	return ((char *)new);
}
