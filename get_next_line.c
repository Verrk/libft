/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   getnextline.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/09/23 11:58:10 by cpestour          #+#    #+#             */
/*   Updated: 2014/12/08 19:47:49 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static t_buffer	*create_buffer(t_buffer *prev, int fd)
{
	t_buffer	*buffer;

	buffer = (t_buffer *)malloc(sizeof(t_buffer));
	if (buffer)
	{
		buffer->fd = fd;
		buffer->buf = NULL;
		buffer->next = NULL;
		if (prev)
			prev->next = buffer;
	}
	return (buffer);
}

static t_buffer	*find_buf(t_buffer *buffer, int fd)
{
	while (buffer->next)
	{
		if (buffer->fd == fd)
			return (buffer);
		buffer = buffer->next;
	}
	return (create_buffer(buffer, fd));
}

static char		*stock_buf(char **line, char *buf, int *i)
{
	while (*buf && *buf != '\n')
	{
		(*line)[*i] = *buf;
		(*i)++;
		buf++;
	}
	(*line)[*i] = '\0';
	if (*buf)
		return (buf + 1);
	return (buf - ft_strlen(*line));
}

static int		read_buf(int fd, char **line, char **buf, int ret)
{
	int			ok;
	int			i;

	ok = 0;
	i = 0;
	if (*buf == NULL)
	{
		*buf = ft_strnew(BUF_SIZE);
		ret = read(fd, *buf, BUF_SIZE);
	}
	*line = ft_strnew(BUF_SIZE + 4096);
	while (!ok && ret > 0)
	{
		if (ft_strchr(*buf, '\n') != NULL)
			ok = 1;
		*buf = stock_buf(line, *buf, &i);
		if (!ok)
		{
			*buf = ft_strnew(BUF_SIZE);
			ret = read(fd, *buf, BUF_SIZE);
		}
	}
	return (ret);
}

int				get_next_line(int const fd, char **line)
{
	static t_buffer	*buffer = NULL;
	t_buffer		*tmp;
	int				ret;

	if (buffer == NULL)
		buffer = create_buffer(NULL, fd);
	tmp = find_buf(buffer, fd);
	ret = BUF_SIZE;
	if (fd < 0)
		return (-1);
	ret = read_buf(fd, line, &(tmp->buf), ret);
	if (ret == 0)
	{
		free(tmp->buf);
		tmp->buf = NULL;
		if (*line[0])
			return (1);
		else
			return (0);
	}
	return (1);
}
